package com.nate;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import static com.nate.Repper.CLASSNAME;

public class CustomClassLoader extends ClassLoader {

    public CustomClassLoader() {}

    public Class loadClazz(byte[] bytes) {
       return defineClass(CLASSNAME, bytes, 0, bytes.length);
    }

    public boolean classInJVM() {
        Class clazz = findLoadedClass(CLASSNAME);
        return clazz != null ? true : false;
    }

    @Override
    public Class findClass(String name) throws ClassNotFoundException{
        byte[] b = null;
        try {
            b = loadClassFromFile(name);
        } catch (Exception e ) {
            System.out.println("In Catch.... Cant' loadClassFromFile()");
        }
        for (byte by : b) {
            System.out.print((char)by);
        }
        return defineClass(name, b, 0, b.length);
    }

    private byte[] loadClassFromFile(String name) throws Exception {
        InputStream inputStream = new FileInputStream(getFile());
        byte[] buffer;
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        int nextValue = 0;
        try {
            while ( (nextValue = inputStream.read()) != -1) {
                byteStream.write(nextValue);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        buffer = byteStream.toByteArray();
        return buffer;
    }

    private File getFile() {
       File currentDir = new File(".") ;
       return new File(currentDir.getParent(), "test.java");
    }
}

