package com.nate;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Repper {

    public static final String CLASSNAME = "repl";

    private final CustomClassLoader classLoader;
    private final JavaCompiler compiler;
    private final String playground = CLASSNAME + ".java";
    private final String playgroundClass = CLASSNAME + ".class";

    private Repper(CustomClassLoader classLoader, JavaCompiler compiler) {
        this.classLoader = classLoader;
        this.compiler = compiler;
    }

    public static Repper create() {
        CustomClassLoader loader = new CustomClassLoader();
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        return new Repper(loader, compiler);
    }

    private int compileFile(String javaFile) {
        return compiler.run(null, null, null, javaFile);
    }


    public void runRepl() throws Exception {
        writeJavaFile();
        System.out.println(classLoader.classInJVM());
        int result = compileFile(playground);
        if (result != 0) {
            throw new RuntimeException("Could not compile file ");
        }
        System.out.println(classLoader.classInJVM());
        Class myClass = loadClass();
        System.out.println(classLoader.classInJVM());
        invokeMethods(myClass);
    }

    private void invokeMethods(Class clazz) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor constructor = clazz.getConstructor();
        constructor.setAccessible(true);
        Object obj = constructor.newInstance(null);
        Method[] methods = clazz.getMethods();
        for (Method meth : methods) {
            if (meth.getName().equals("printTest")) {
                meth.setAccessible(true);
                meth.invoke(obj);
            }
        }
    }

    private Class loadClass() throws IOException {
        Path path = Paths.get(playgroundClass);
        byte[] bytes = Files.readAllBytes(path);
        return classLoader.loadClazz(bytes);
    }

    public void writeJavaFile() throws IOException {
        String testString = "class repl { public repl() {} public void printTest(){ System.out.println(\"yoooo\");} @Override public String toString() {return \"from toString()\";}}";
        String filename = "repl.java";
        OutputStream outputStream = new FileOutputStream(filename);
        outputStream.write(testString.getBytes());
        outputStream.flush();
        outputStream.close();
    }

    public void repl() {
       /* Take input from command line
        - Parse line, if it is valid one liner, then execute command
            - If it's not one liner, store in buffer and keep reading next input line
        */

    }

}
